#include <iostream>
#include <string>
#include "MemoryAccess.h"
#include "AlbumManager.h"
#include "DataBaseAccess.h"
#include "DataAccessTest.h"

#include <ctime>

#define WELCOME " Welcome to Gallery! by"
#define YOUR_NAME "Eliran Megidsih"

void opening();

int getCommandNumberFromUser()
{
	std::string message("\nPlease enter any command(use number): ");
	std::string numericStr("0123456789");

	std::cout << message << std::endl;
	std::string input;
	std::getline(std::cin, input);

	while (std::cin.fail() || std::cin.eof() || input.find_first_not_of(numericStr) != std::string::npos) {

		std::cout << "Please enter a number only!" << std::endl;

		if (input.find_first_not_of(numericStr) == std::string::npos) {
			std::cin.clear();
		}

		std::cout << std::endl << message << std::endl;
		std::getline(std::cin, input);
	}

	return std::atoi(input.c_str());
}

int main(void)
{
	DataBaseAccess dataAccess ;
	AlbumManager albumManager(dataAccess);
	std::string albumName;
	opening();
	std::cout << " =====================================" << std::endl;
	std::cout << " Type " << HELP << " to a list of all supported commands" << std::endl;

	do {
		int commandNumber = getCommandNumberFromUser();

		try {
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		}
		catch (std::exception & e) {
			std::cout << e.what() << std::endl;
		}
	} while (true);
	
	
}

void opening()
{
	time_t now = time(0);
	tm* ltm = localtime(&now);
	std::cout << " " << ltm->tm_mday << "/" << 1 + ltm->tm_mon << "/" << 1900 + ltm->tm_year << std::endl;
	std::cout << WELCOME << YOUR_NAME << std::endl;
}
