#pragma once

#include "IDataAccess.h"
#include "DataAccessTest.h"
#include "ItemNotFoundException.h"

class DataBaseAccess : public IDataAccess
{
public:

	sqlite3 & get_db() const;
	DataBaseAccess()=default;
	virtual ~DataBaseAccess() = default;
	int sendData(const char* query, int (*callback)(void*, int, char**, char**));
	bool open() override;
	void close() override;
	void clear() override;
	void closeAlbum(Album& album);

	void deleteAlbum(const std::string& albumName,int userId)override;
	
	void addPictureToAlbumByName(const std::string& albumName,const Picture& picture)override;
	
	void removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)override;
	
	void tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)override;
	void untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)override;
	void createUser(User& user)override;
	void deleteUser(const User& user)override;
	float averageTagsPerAlbumOfUser(const User& user)override;
	

	//lab3

	void initListAlbums();
	void initListUsers();

	static int callbackAlbum(void* data, int argc, char** argv, char** azColName);
	static int callbackUser(void* data, int argc, char** argv, char** azColName);
	static int callbackPictures(void* data, int argc, char** argv, char** azColName);

	const std::list<Album> getAlbums() override;

	const std::list<Album> getAlbumsOfUser(const User& user) override;
	void createAlbum(const Album& album) override;
	bool doesAlbumExists(const std::string& albumName, int userId) override;
	Album openAlbum(const std::string& albumName) override;
	void printAlbums() override;

	User getUser(int userId) override;
	void printUsers() override;
	bool doesUserExists(int userId) override;


	int countAlbumsOwnedOfUser(const User& user) override;
	int countAlbumsTaggedOfUser(const User& user) override;
	int countTagsOfUser(const User& user) override;
	

	User getTopTaggedUser() override;
	Picture getTopTaggedPicture() override;
	std::list<Picture> getTaggedPicturesOfUser(const User& user) override;


private:
	sqlite3 * _db;
	std::string _dbFileName = "MyDB.sqlite";
	std::string _query;

	std::list<Album> _ListAlbums;
	std::list<User>  _ListUsers;
};