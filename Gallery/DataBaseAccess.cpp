#include "DataBaseAccess.h"


sqlite3& DataBaseAccess::get_db()const
{
	return *(this->_db);
}

int DataBaseAccess::sendData(const char* query, int (*callback)(void*, int, char**, char**))
{
	std::cout << query << std::endl;
	int res = sqlite3_exec(this->_db, query, callback, nullptr, nullptr);
	return res;
}


bool DataBaseAccess::open()
{
	int res = sqlite3_open(this->_dbFileName.c_str(), &this->_db);
	if (res != SQLITE_OK)
	{
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	return true;
}


void DataBaseAccess::close()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}


void DataBaseAccess::clear()
{
	this->_ListAlbums.clear();
	this->_ListUsers.clear();
}


void DataBaseAccess::closeAlbum(Album& album)
{
	//not allocated memory
}


void DataBaseAccess::deleteAlbum(const std::string& albumName, int userId)// i need to remove pictures and tags
{

	//remove all tags
	this->_query = "DELETE FROM TAGS "
		"WHERE TAGS.USER_ID IN(SELECT TAGS.USER_ID FROM TAGS "
		"INNER JOIN ALBUMS ON TAGS.USER_ID = ALBUMS.USER_ID "
		"INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID "
		"WHERE ALBUMS.NAME = '" + albumName + "' AND TAGS.USER_ID = " + std::to_string(userId) + " AND ALBUMS.USER_ID = " + std::to_string(userId) + " );";
	sendData(this->_query.c_str(), nullptr);

	//remove all pictures that in album
	this->_query = "DELETE FROM PICTURES "
		"WHERE PICTURES.ALBUM_ID IN( "
		"SELECT ALBUM_ID FROM PICTURES "
		"INNER JOIN ALBUMS "
		"ON ALBUMS.ID = PICTURES.ALBUM_ID "
		"WHERE ALBUMS.USER_ID = " + std::to_string(userId) + " AND ALBUMS.NAME = '" + albumName + "' AND  PICTURES.ALBUM_ID = ALBUMS.ID);";

	sendData(this->_query.c_str(), nullptr);

	this->_query = "DELETE FROM ALBUMS"
		" WHERE USER_ID = " + std::to_string(userId) + " AND NAME = '" + albumName + "';";
	sendData(this->_query.c_str(), nullptr);
}


void DataBaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	this->_query = " INSERT INTO PICTURES(ID,NAME,LOCATION,CREATION_DATE,ALBUM_ID)"
		" SELECT " + std::to_string(picture.getId()) + ",'" + picture.getName() + "', '" + picture.getPath() + "' ,'" + picture.getCreationDate() + "' ,ALBUMS.ID"
		" FROM ALBUMS WHERE(ALBUMS.NAME= '" + albumName + "');";
	sendData(this->_query.c_str(), nullptr);
}


void DataBaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)// i need to remove tags
{
	//remove tags 
	this->_query = "DELETE FROM TAGS "
		"WHERE TAGS.USER_ID "
		"IN(SELECT TAGS.USER_ID FROM TAGS "
		"INNER JOIN ALBUMS ON TAGS.USER_ID = ALBUMS.USER_ID "
		"INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID "
		"WHERE ALBUMS.NAME = '" + albumName + "' AND PICTURES.NAME = '" + pictureName + "' );";

	this->_query = " DELETE FROM PICTURES"
		" WHERE EXISTS"
		" (SELECT * FROM ALBUMS"
		" WHERE ALBUMS.name = '" + albumName + "' AND PICTURES.NAME ='" + pictureName + "' );";
	sendData(this->_query.c_str(), nullptr);
}


void DataBaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	this->_query = " INSERT INTO TAGS(PICTURE_ID, USER_ID)"
		" SELECT PICTURES.ID, USERS.ID FROM PICTURES, ALBUMS, USERS"
		" WHERE ALBUMS.USER_ID = " + std::to_string(userId) + " AND PICTURES.NAME = '" + pictureName + "' AND ALBUMS.NAME = '" + albumName + "';";
	sendData(this->_query.c_str(), nullptr);
}


void DataBaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{

	this->_query = "DELETE FROM TAGS "
		"WHERE TAGS.USER_ID IN(SELECT TAGS.USER_ID FROM TAGS "
		"INNER JOIN ALBUMS ON TAGS.USER_ID = ALBUMS.USER_ID "
		"INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID "
		"WHERE ALBUMS.NAME = '" + albumName + "' AND PICTURES.NAME = '" + pictureName + "' AND TAGS.USER_ID = " + std::to_string(userId) + " AND ALBUMS.USER_ID = " + std::to_string(userId) + " );";
	sendData(this->_query.c_str(), nullptr);
}


void DataBaseAccess::createUser(User& user)
{
	this->_query = "INSERT INTO USERS VALUES(" + std::to_string(user.getId()) + " , '" + user.getName() + "' );";
	sendData(this->_query.c_str(), nullptr);
}


void DataBaseAccess::deleteUser(const User& user)
{
	this->_query = "DELETE FROM USERS WHERE ID= " + std::to_string(user.getId()) + ";";
	sendData(this->_query.c_str(), nullptr);
}


float DataBaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	this->_query = "SELECT(SELECT COUNT(*) FROM TAGS WHERE USER_ID = " + std::to_string(user.getId()) + " )"
		"/ (SELECT COUNT(*) FROM ALBUMS WHERE ALBUMS.USER_ID = " + std::to_string(user.getId()) + " );";
	sendData(this->_query.c_str(), nullptr);

	return 0.0f;
}

void DataBaseAccess::initListAlbums()
{
	this->_ListAlbums.clear();
	this->_query = "SELECT * FROM ALBUMS;";
	int res = sqlite3_exec(this->_db, _query.c_str(), DataBaseAccess::callbackAlbum, &(this->_ListAlbums), nullptr);
}

void DataBaseAccess::initListUsers()
{
	this->_ListUsers.clear();
	this->_query = "SELECT * FROM USERS ;";
	int res = sqlite3_exec(this->_db, _query.c_str(), DataBaseAccess::callbackUser, &(this->_ListUsers), nullptr);
}

int DataBaseAccess::callbackAlbum(void* data, int argc, char** argv, char** azColName)
{
	Album album;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "NAME")
		{
			album.setName(argv[i]);
		}
		else if (std::string(azColName[i]) == "CREATION_DATE")
		{
			album.setCreationDateNow();
		}
		else if (std::string(azColName[i]) == "USER_ID")
		{
			album.setOwner(atoi(argv[i]));
		}
	}
	((std::list<Album>*)data)->push_back(album);
	return 0;
}

int DataBaseAccess::callbackUser(void* data, int argc, char** argv, char** azColName)
{
	std::string name = "";
	const std::string name1 = "";
	int userId = 0;
	if (argc == 0)
	{
		throw ItemNotFoundException("User", *((int*)data));
	}
	else
	{
		for (int i = 0; i < argc; i++)
		{

			if (std::string(azColName[i]) == "NAME")
			{
				name = argv[i];
			}
			else if (std::string(azColName[i]) == "ID")
			{
				userId = atoi(argv[i]);
			}
		}
		User user(userId, name);
		((std::list<User>*)data)->push_back(user);
	}
	return 0;
}

int DataBaseAccess::callbackPictures(void* data, int argc, char** argv, char** azColName)
{
	std::string name = "";
	std::string creationDate = "";
	std::string location = "";
	int pictureId = 0;
	int i = 0;
	if (argc == 0)
	{
		std::cout << "Error"<<std::endl;
	}
	else
	{
		for (i = 0; i < argc; i++)
		{
			if (std::string(azColName[i]) == "ID")
			{
				pictureId = atoi(argv[i]);
			}
			else if (std::string(azColName[i]) == "NAME")
			{
				name = argv[i];
			}
			else if (std::string(azColName[i]) == "LOCATION")
			{
				location = argv[i];
			}
			else if (std::string(azColName[i]) == "CREATION_DATE")
			{
				creationDate = argv[i];
			}
		}
		Picture p(pictureId, name, location, creationDate);
		((Album*)data)->addPicture(p);

	}
	return 0;
}


const std::list<Album> DataBaseAccess::getAlbums()
{
	DataBaseAccess::initListAlbums();
	return this->_ListAlbums;
}


const std::list<Album> DataBaseAccess::getAlbumsOfUser(const User& user)
{

	this->_ListAlbums.clear();
	this->_query = "SELECT * FROM ALBUMS WHERE USER_ID = " + std::to_string(user.getId()) + " ;";
	int res = sqlite3_exec(this->_db, _query.c_str(), DataBaseAccess::callbackAlbum, &(this->_ListAlbums), nullptr);
	for (auto it = this->_ListAlbums.begin(); it != this->_ListAlbums.end(); ++it)
	{
		std::cout << *it << std::endl;
	}

	return this->_ListAlbums;
}


void DataBaseAccess::createAlbum(const Album& album)
{

	this->_query = "INSERT INTO ALBUMS VALUES("
		+ std::to_string(11) + ", '" + album.getName() + "' , '" + std::to_string(album.getOwnerId()) + "' , '" + album.getCreationDate() + "');";
	sendData(this->_query.c_str(), nullptr);
}


bool DataBaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	DataBaseAccess::initListAlbums();
	for (auto it = this->_ListAlbums.begin(); it != this->_ListAlbums.end(); ++it)
	{
		if ((it->getName() == albumName) && (it->getOwnerId() == userId))
		{
			return true;
		}
	}
	return false;
}


Album DataBaseAccess::openAlbum(const std::string& albumName)
{
	DataBaseAccess::initListAlbums();
	for (auto& album : this->_ListAlbums)
	{
		if (albumName == album.getName())
		{
			album.getPictures().clear(); // clear list pictures in albu
			this->_query = "SELECT PICTURES.ID, PICTURES.NAME, PICTURES.LOCATION, PICTURES.CREATION_DATE FROM PICTURES, ALBUMS "
				"WHERE ALBUMS.USER_ID = " + std::to_string(album.getOwnerId()) + " AND ALBUMS.name = '" + album.getName() + "' AND ALBUMS.ID=PICTURES.ALBUM_ID ;";
			std::cout << _query << std::endl;
			int res = sqlite3_exec(this->_db, _query.c_str(), DataBaseAccess::callbackPictures, &album, nullptr);
			return album;
		}
	}
	throw MyException("No album with name " + albumName + " exists");
}


void DataBaseAccess::printAlbums()
{
	DataBaseAccess::initListAlbums();
	for (auto it = this->_ListAlbums.begin(); it != this->_ListAlbums.end(); ++it)
	{
		std::cout << it->getName() << std::endl;
	}
}


User DataBaseAccess::getUser(int userId)
{

	this->_ListUsers.clear();
	this->_query = "SELECT * FROM USERS WHERE ID = " + std::to_string(userId) + " ;";
	int res = sqlite3_exec(this->_db, _query.c_str(), DataBaseAccess::callbackUser, &(this->_ListUsers), nullptr);
	return this->_ListUsers.back();
}


void DataBaseAccess::printUsers()
{
	DataBaseAccess::initListUsers();
	for (auto it = this->_ListUsers.begin(); it != this->_ListUsers.end(); ++it)
	{
		std::cout << "ID:" << it->getId() << " Name:" << it->getName() << std::endl;
	}
}


bool DataBaseAccess::doesUserExists(int userId)
{
	DataBaseAccess::initListUsers();
	for (auto it = this->_ListUsers.begin(); it != this->_ListUsers.end(); ++it)
	{
		if (it->getId() == userId)
		{
			return true;
		}
	}
	return false;
}



int DataBaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	DataBaseAccess::initListAlbums();

	int albumsCount = 0;
	for (const auto& album : this->_ListAlbums)
	{
		if (album.getOwnerId() == user.getId())
		{
			++albumsCount;
		}
	}
	return albumsCount;
}


int DataBaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	int albumsCount = 0;

	DataBaseAccess::initListAlbums();
	for (const auto& album : this->_ListAlbums)
	{
		const std::list<Picture>& pics = album.getPictures();

		for (const auto& picture : pics)
		{
			if (picture.isUserTagged(user))
			{
				albumsCount++;
				break;
			}
		}
	}

	return albumsCount;
}

int DataBaseAccess::countTagsOfUser(const User& user)
{
	DataBaseAccess::initListAlbums();

	int tagsCount = 0;

	for (const auto& album : this->_ListAlbums)
	{
		const std::list<Picture>& pics = album.getPictures();

		for (const auto& picture : pics)
		{
			if (picture.isUserTagged(user))
			{
				tagsCount++;
			}
		}
	}

	return tagsCount;
}


User DataBaseAccess::getTopTaggedUser()
{
	DataBaseAccess::initListUsers();
	for (const auto& user : _ListUsers) {
		if (user.getId() == 1) {
			return user;
		}
	}
}

Picture DataBaseAccess::getTopTaggedPicture()
{
	DataBaseAccess::initListAlbums();

	int currentMax = -1;
	const Picture* mostTaggedPic = nullptr;
	for (const auto& album : _ListAlbums) {
		for (const Picture& picture : album.getPictures()) {
			int tagsCount = picture.getTagsCount();
			if (tagsCount == 0) {
				continue;
			}

			if (tagsCount <= currentMax) {
				continue;
			}

			mostTaggedPic = &picture;
			currentMax = tagsCount;
		}
	}
	if (nullptr == mostTaggedPic)
	{
		throw MyException("There isn't any tagged picture.");
	}
	return *mostTaggedPic;
}


std::list<Picture> DataBaseAccess::getTaggedPicturesOfUser(const User& user)
{
	std::list<Picture> pictures;

	for (const auto& album : this->_ListAlbums)
	{
		for (const auto& picture : album.getPictures())
		{
			if (picture.isUserTagged(user))
			{
				pictures.push_back(picture);
			}
		}
	}
	return pictures;
}


